from sys import maxsize
import inquirer
import benchmarks.benchmark1 as benchmark1
import benchmarks.benchmark2 as benchmark2
import benchmarks.benchmark3 as benchmark3
from config import Config
from algorithm import execute
from pprint import pprint
import math
from schema import validate_json, validate_execution_config
import os
import json
import cProfile

with open("./execution_config.json", "r") as file:
    try:
        json_data = json.load(file)
        if validate_execution_config(json_data):
            Config.repair = json_data["repair_level"]
            Config.n_gen = json_data["generations"]
            Config.algorithm = json_data["algorithm"]
            Config.multithreading = json_data["multithreading"]
            Config.n_threads = json_data["n_threads"]
        else:
            raise ValueError("Configuration error")
    except:
        raise ValueError("Configuration error")

folder_path = 'input'  # Path to the input folder
file_list = []

for filename in os.listdir(folder_path):
    file_path = os.path.join(folder_path, filename)
    if os.path.isfile(file_path) and filename.endswith('.json'):
        with open(file_path, 'r') as file:
            try:
                json_data = json.load(file)
                if validate_json(json_data):
                    file_list.append(json_data)
            except json.JSONDecodeError:
                pass

print("Genetic Algorithm for the CHDSP problem\n")
questions = [
    inquirer.List(
        "choice",
        message=
        "Would you like to import a configuration or use a default benchmark?",
        choices=[("Import configuration", True), ("Benchmark", False)],
    ),
]
answers = inquirer.prompt(questions)
inputConfig = None
if answers["choice"] == True:
    if file_list:
        questions = [
            inquirer.List(
                "configuration",
                message="Select a configuration",
                choices=[(data['name'], i)
                         for i, data in enumerate(file_list)],
            )
        ]
        answers = inquirer.prompt(questions)
        inputConfig = file_list[answers["configuration"]]
    else:
        print('No valid configuration was found in the input folder')
else:
    questions = [
        inquirer.List(
            "benchmark",
            message="Select a benchmark",
            choices=[("Phil", 0), ("Ligne 1", 1), ("Ligne 2", 2)],
        )
    ]
    answers = inquirer.prompt(questions)
    benchmark = [benchmark1, benchmark2, benchmark3][answers["benchmark"]]
    inputConfig = vars(benchmark1)
if inputConfig:
    Config.name = inputConfig['name']
    Config.N = inputConfig['N']
    Config.inf = inputConfig['inf']
    Config.m = inputConfig['m']
    Config.M = inputConfig['M']
    Config.r = inputConfig['r']
    Config.d = inputConfig['d']
    Config.separator = inputConfig['separator']
    Config.NS = math.floor((inputConfig['N'] - 2) / 2)
    p = cProfile.Profile()
    p.enable()
    execute()
    p.disable()
    p.dump_stats("results.prof")