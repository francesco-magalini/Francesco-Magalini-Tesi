# from math import inf
import math

# benchmark "Phil"
# unità di tempo s (secondi)
name = "Phil"
nop = 14
mt = 12
N = 13
inf = 10000

# tempi minimi di ammollo
# 14
m = [120, 150, 90, 120, 90, 30, 60, 60, 45, 130, 120, 90, 30, 0]

# tempi massimi di ammollo
# 14
M = [inf, 200, 120, 180, 125, 40, 120, 120, 75, inf, inf, 120, 60, inf]

# r[i] tempo necessario per un paranco per muoversi da carico dalla vasca i alla vasca i + 1
# 13
r = [31, 22, 22, 22, 25, 23, 22, 22, 22, 47, 27, 22, 30]

# d[i][j] tempo necessario per un paranco per muoversi senza carico dalla vasca i alla j
# 13 x 13
d = [
    [0, 11, 14, 16, 14, 19, 22, 24, 26, 29, 6, 8, 10],
    [11, 0, 2, 5, 2, 8, 10, 13, 15, 17, 10, 3, 1],
    [14, 2, 0, 2, 0, 5, 8, 10, 13, 15, 12, 6, 3],
    [16, 5, 2, 0, 2, 3, 5, 8, 10, 13, 15, 8, 6],
    [14, 2, 0, 2, 0, 5, 8, 10, 13, 15, 12, 6, 3],
    [19, 8, 5, 3, 5, 0, 3, 5, 7, 10, 18, 11, 9],
    [22, 10, 8, 5, 8, 3, 0, 2, 5, 7, 20, 14, 11],
    [24, 13, 10, 8, 10, 5, 2, 0, 2, 5, 23, 16, 14],
    [26, 15, 13, 10, 13, 7, 5, 2, 0, 2, 25, 19, 16],
    [29, 17, 15, 13, 15, 10, 7, 5, 2, 0, 27, 21, 19],
    [6, 10, 12, 15, 12, 18, 20, 23, 25, 27, 0, 7, 9],
    [8, 3, 6, 8, 6, 11, 14, 16, 19, 21, 7, 0, 2],
    [10, 1, 3, 6, 3, 9, 11, 14, 16, 19, 9, 2, 0],
]

separator = 0
NS = math.floor((N - 2) / 2)
