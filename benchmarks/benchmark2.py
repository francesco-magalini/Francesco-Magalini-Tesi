# from math import inf
import math

# benchmark "Ligne1"
# unità di tempo s (secondi)
name = "Ligne1"
nop = 14
mt = 12
N = 13
inf = 10000

# tempi minimi di ammollo
# 14
m = [180, 60, 30, 120, 180, 30, 30, 60, 60, 300, 60, 60, 120, 180]

# tempi massimi di ammollo
# 14
M = [inf, 120, 90, 240, 240, 90, 90, 120, 120, 420, 120, 120, 180, inf]

# r[i] tempo necessario per un paranco per muoversi da carico dalla vasca i alla vasca i + 1
# 13
r = [23, 29, 19, 24, 19, 19, 24, 24, 20, 24, 24, 19, 30, 23]

# d[i][j] tempo necessario per un paranco per muoversi senza carico dalla vasca i alla j
# 13 x 13
d = [
    [0, 13, 12, 11, 10, 9, 9, 8, 8, 6, 6, 5, 5],
    [13, 0, 4, 5, 6, 7, 7, 8, 9, 10, 10, 11, 12],
    [12, 4, 0, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11],
    [11, 5, 4, 0, 4, 5, 6, 6, 7, 8, 9, 10, 10],
    [10, 6, 5, 4, 0, 4, 5, 6, 6, 7, 8, 9, 9],
    [9, 7, 6, 5, 4, 0, 4, 5, 5, 6, 7, 8, 8],
    [9, 7, 6, 6, 5, 4, 0, 4, 4, 5, 6, 7, 8],
    [8, 8, 7, 6, 6, 5, 4, 0, 4, 4, 5, 6, 7],
    [8, 9, 8, 7, 6, 5, 4, 4, 0, 5, 5, 6, 7],
    [6, 10, 9, 8, 7, 6, 5, 4, 5, 0, 4, 5, 5],
    [6, 10, 9, 9, 8, 7, 6, 5, 5, 4, 0, 4, 5],
    [5, 11, 10, 10, 9, 8, 7, 6, 6, 5, 4, 0, 4],
    [5, 12, 11, 10, 9, 8, 8, 7, 7, 5, 5, 4, 0],
]

separator = 0
NS = math.floor((N - 2) / 2)
