from jsonschema import validate

json_schema = {
    "type": "object",
    "properties": {
        "N": {
            "type": "integer",
            "minimum": 3
        },
        "inf": {
            "type": "integer",
            "minimum": 1
        },
        "separator": {
            "type": "integer"
        },
        "name": {
            "type": "string",
            "minLength": 1,
        },
        "m": {
            "type": "array",
            "items": {
                "type": "integer",
                "minimum": 0
            },
            "minItems": 1
        },
        "M": {
            "type": "array",
            "items": {
                "type": "integer",
                "minimum": 0
            },
            "minItems": 1
        },
        "r": {
            "type": "array",
            "items": {
                "type": "integer",
                "minimum": 0
            },
            "minItems": 1
        },
        "d": {
            "type": "array",
            "items": {
                "type": "array",
                "items": {
                    "type": "integer",
                    "minimum": 0
                },
                "minItems": 1
            },
            "minItems": 1
        }
    },
    "required": ["N", "inf", "separator", "name", "m", "M", "r", "d"],
    "additionalProperties": False
}

execution_schema = {
    "type":
    "object",
    "properties": {
        "repair_level": {
            "type": "integer",
            "minimum": 0,
            "maximum": 2
        },
        "generations": {
            "type": "integer",
            "minimum": 2
        },
        "algorithm": {
            "enum": ["NSGA2", "UNSGA3"]
        },
        "multithreading": {
            "type": "boolean"
        },
        "n_threads": {
            "type": "integer",
            "minimum": 2
        },
    },
    "required": [
        "repair_level", "generations", "algorithm", "multithreading",
        "n_threads"
    ],
    "additionalProperties":
    False
}


def validate_json(obj):
    flag = False
    try:
        validate(instance=obj, schema=json_schema)
        if len(obj["m"]) != obj["N"] + 1: raise ValueError("Invalid m length")
        if len(obj["M"]) != obj["N"] + 1: raise ValueError("Invalid M length")
        if len(obj["r"]) != obj["N"]: raise ValueError("Invalid r length")
        if len(obj["d"]) != obj["N"]: raise ValueError("Invalid d length")
        for arr in obj["d"]:
            if len(arr) != obj["N"]: raise ValueError("Invalid d length")
        if obj["separator"] in range(1, obj["N"] + 1):
            raise ValueError("Invalid separator")
        flag = True
    except Exception as e:
        print(f"Error: {e}")
    return flag


def validate_execution_config(obj):
    flag = False
    try:
        validate(instance=obj, schema=execution_schema)
        flag = True
    except Exception as e:
        print(f"Error: {e}")
    return flag