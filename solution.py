# Questo file contiene l'implementazione di una singola soluzione rappresentata dal cromosoma
# Contiene una serie di variabili, una procedura di decodifica e di valutazione
import numpy as np
from ortools.linear_solver import pywraplp
import itertools
from collections import Counter
from config import Config
from pprint import pprint


class Solution:

    def __init__(self, chromosome):
        self.chromosome = np.copy(chromosome)
        self.status = (
            "initialized"  # initialized, valid, invalid, evaluated, not evaluated
        )
        self.constructSolution()

    def __str__(self):
        str = f"Chromosome: {self.chromosome}\n"
        if self.status == "valid" or self.status == "evaluated":
            if self.status == "evaluated":
                str += f"Period: {self.period}\n" f"t: {self.t}\n"
            str += (f"Hoists {self.mh}\n"
                    f"Split chromosome: {self.splitChromosome}\n"
                    f"Empty moves: {self.emptyMoves}\n"
                    f"Empty hoist moves: {self.hoistEmptyMoves}\n"
                    f"Empty hoist moves boolean: {self.Z}\n"
                    f"Transport hoist moves: {self.hoistTransportMoves}\n"
                    f"Hoist moves {self.hoistMoves}\n\n")
        else:
            str += f"Status: {self.status}\n\n"
        return str

    def toDict(self):
        d = dict(self.__dict__)
        d["chromosome"] = d["chromosome"].tolist()
        if "Z" in d:
            d["Z"] = d["Z"].tolist()
        if "emptyMoves" in d:
            d["emptyMoves"] = list(d["emptyMoves"])
        return d

    # Genera la soluzione a partire dal cromosoma
    def constructSolution(self):
        mh = 0
        try:
            ns = np.count_nonzero(self.chromosome == Config.separator)
            S = self.chromosome.size
            # controllo numero massimo di zeri
            if ns > Config.NS:
                raise ValueError("Errore costruzione soluzione")
            # se la lunghezza del cromosoma è minore di 2 o maggiore di N + numero massimo di zeri,
            # il cromosoma viene scartato
            if not 2 <= S - ns <= Config.N:
                raise ValueError("Errore costruzione soluzione")
            if [
                    item for item, count in Counter(self.chromosome).items()
                    if count > 1 and item != Config.separator
            ]:
                raise ValueError("Errore costruzione soluzione")
            # divide il cromosoma in liste divise dal separatore
            self.splitChromosome = [
                list(map(int, y)) for x, y in itertools.groupby(
                    self.chromosome, lambda z: z == Config.separator) if not x
            ]
            # calcolo delle mosse senza carico
            # ogni elemento delle liste calcolate in precedenza rappresenta la mossa senza carico
            # (i, j) dove i è l'elemento e j il suo successivo
            # in caso i sia l'ultimo elemento della lista, j è il primo
            emptyMoves = {(array[i], array[(i + 1) % len(array)])
                          for array in self.splitChromosome
                          for i in range(len(array))}
            # calcolo delle mosse senza carico fittizie
            # se delle vasche non sono presenti nel cromosoma vengono aggiunte come
            # mosse senza carico fittizie (i, i)
            for i in filter(lambda x: x not in self.chromosome,
                            range(1, Config.N + 1)):
                emptyMoves.add((i, i))
            # Mosse senza carico
            self.emptyMoves = emptyMoves.copy()
            emptyMovesLookup = {x: y for x, y in emptyMoves}
            self.emptyMovesLookup = emptyMovesLookup.copy()
            mh = 1
            m = 2
            j = 2
            # Mosse (sia con carico che senza) diviso per paranco
            self.hoistMoves = [[]]
            self.hoistEmptyMoves = [[]]
            self.hoistTransportMoves = [[]]
            # calcolo delle mosse completo
            while emptyMoves:
                k = emptyMovesLookup[j]
                del emptyMovesLookup[j]
                emptyMoves.remove((j, k))
                self.hoistMoves[mh - 1].append(
                    (Config.N if j == 1 else j - 1, j))
                self.hoistTransportMoves[mh - 1].append(
                    (Config.N if j == 1 else j - 1, j))
                self.hoistMoves[mh - 1].append((j, k))
                self.hoistEmptyMoves[mh - 1].append((j, k))
                j = 1 if k == Config.N else k + 1
                if j == m and emptyMoves:
                    mh = mh + 1
                    self.hoistMoves.append([])
                    self.hoistEmptyMoves.append([])
                    self.hoistTransportMoves.append([])
                    m = min(emptyMoves, key=lambda x: x[0])[0]
                    j = m
            # Numero di paranchi
            self.mh = len(self.hoistMoves)
            self.z = [len(l) for l in self.hoistEmptyMoves]
            self.Z = np.array(
                [[(i, self.emptyMovesLookup[i]) in self.hoistEmptyMoves[h]
                  for i in range(1, Config.N + 1)] for h in range(self.mh)],
                dtype=int,
            )
            self.status = "valid"
        except ValueError:
            self.status = "invalid"
        except Exception as e:
            self.status = "invalid"
            # pprint(self.chromosome)
            # pprint(e)

    # Calcola il periodo (e altre variabili) della soluzione generata
    def evaluate(self):
        if self.status != "valid":
            return
        solver = pywraplp.Solver.CreateSolver('SAT')
        #print(Config.__dict__)
        infinity = Config.inf
        N = Config.N
        m = Config.m
        M = Config.M
        r = Config.r
        d = Config.d
        epsilon = 0.1
        # solver.set_time_limit(30 * 1000)
        # solver.EnableOutput()
        T = solver.IntVar(0, infinity, "Period")
        t = [
            solver.IntVar(
                0, infinity,
                f"Tempo di inizio spostamento senza carico da {i + 1}")
            for i in range(N)
        ]
        b = [
            solver.IntVar(0, 1,
                          f"Carico abbassato e tolto nella vasca {i + 1}")
            for i in range(N)
        ]
        a = [[
            solver.IntVar(
                0,
                1,
                f"Spostamento di carico della mossa {u + 1} eseguiti dal paranco {h + 1} nello stesso periodo",
            ) for u in range(self.z[h])
        ] for h in range(self.mh)]
        y = [[
            solver.IntVar(
                0, 1,
                f"Spostamento con carico {i} eseguito prima di quello {j}")
            for j in range(N)
        ] for i in range(N)]
        # 2
        for i in range(N):
            solver.Add(0 <= t[i])
            solver.Add(t[i] <= T)
        # 3
        solver.Add(t[1] == r[0])
        # 4
        solver.Add(t[0] + m[N] <= T)
        # 5
        for i in range(N):
            solver.Add(m[i] <= T)
        # 6
        for i in range(N - 1):
            solver.Add((b[i] - 1) * infinity <= t[i] - (t[i + 1] - r[i]))
            solver.Add(t[i] - (t[i + 1] - r[i]) <= b[i] * infinity)
        # 7
        solver.Add((b[N - 1] - 1) * infinity <= t[N - 1] - (t[0] - r[N - 1]))
        solver.Add(t[N - 1] - (t[0] - r[N - 1]) <= b[N - 1] * infinity)
        for i in range(N - 1):
            # 8
            solver.Add(m[i] - b[i] * infinity <= t[i + 1] - r[i] - t[i])
            solver.Add(t[i + 1] - r[i] - t[i] <= M[i] + b[i] * infinity)
            # 9
            solver.Add(m[i] +
                       (b[i] - 1) * infinity <= T + t[i + 1] - r[i] - t[i])
            solver.Add(T + t[i + 1] - r[i] - t[i] <= M[i] +
                       (1 - b[i]) * infinity)
        # 10
        solver.Add(
            m[N - 1] - b[N - 1] * infinity <= t[0] - r[N - 1] - t[N - 1])
        solver.Add(
            t[0] - r[N - 1] - t[N - 1] <= M[N - 1] + b[N - 1] * infinity)
        # 11
        solver.Add(m[N - 1] +
                   (b[N - 1] - 1) * infinity <= T + t[0] - r[N - 1] - t[N - 1])
        solver.Add(T + t[0] - r[N - 1] - t[N - 1] <= M[N - 1] +
                   (1 - b[N - 1]) * infinity)
        # 12
        for array in a:
            solver.Add(solver.Sum(array) == 1)
        # 13
        for u in range(self.z[0] - 1):
            solver.Add(a[0][u] == 0)
        solver.Add(a[0][self.z[0] - 1] == 1)
        for h in range(self.mh):
            for u in range(self.z[h]):
                j, i = self.hoistEmptyMoves[h][u]
                # x = i + 1
                # x, k = self.hoistEmptyMoves[h][0 if u == self.z[h] - 1 else u + 1]
                # print((j, i), (x, k))
                j -= 1
                i -= 1
                # 14
                solver.Add(t[j] + d[j][i] <= t[(i + 1) % N] - r[i] +
                           a[h][u] * infinity)
                # 15
                solver.Add(t[j] + d[j][i] <= T + t[(i + 1) % N] - r[i] +
                           (1 - a[h][u]) * infinity)
        for i in range(N):
            for j in range(N):
                if i == j:
                    continue
                # 16
                solver.Add(t[(j + 1) % N] - r[j] -
                           (t[(i + 1) % N] - r[i]) <= infinity *
                           y[(i + 1) % N][(j + 1) % N])
                # 17
                solver.Add(y[i][j] + y[j][i] == 1)
        if self.mh > 1:
            for h in range(self.mh):
                for k in range(self.mh):
                    if h == k:
                        continue
                    for x1 in range(len(self.hoistTransportMoves[h])):
                        for x2 in range(len(self.hoistTransportMoves[k])):
                            i, _ = self.hoistTransportMoves[h][x1]
                            p, _ = self.hoistTransportMoves[h][x1 - 1]
                            j, _ = self.hoistTransportMoves[k][x2]
                            q, _ = self.hoistTransportMoves[k][x2 - 1]
                            i -= 1
                            p -= 1
                            j -= 1
                            q -= 1
                            if k > h and j < i:
                                # 18
                                solver.Add(
                                    t[(i + 1) % N] + d[(i + 1) % N][j] -
                                    t[(q + 1) % N] -
                                    d[(q + 1) % N][j] <= infinity *
                                    (3 - y[(i + 1) % N][
                                        (j + 1) % N] - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(j + 1) % N]) -
                                    epsilon)
                                # 19
                                solver.Add(
                                    t[(j + 1) % N] + d[(j + 1) % N][i] -
                                    t[(p + 1) % N] -
                                    d[(p + 1) % N][i] <= infinity *
                                    (3 - y[(j + 1) % N][
                                        (i + 1) % N] - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(j + 1) % N]) -
                                    epsilon)
                            elif k < h and j > i:
                                # 20
                                solver.Add(
                                    t[(i + 1) % N] + d[(i + 1) % N][j] -
                                    t[(q + 1) % N] -
                                    d[(q + 1) % N][j] <= infinity *
                                    (3 - y[(i + 1) % N][
                                        (j + 1) % N] - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[:h + 1], axis=0)[
                                         (j + 1) % N]) - epsilon)
                                # 21
                                solver.Add(
                                    t[(j + 1) % N] + d[(j + 1) % N][i] -
                                    t[(p + 1) % N] -
                                    d[(p + 1) % N][i] <= infinity *
                                    (3 - y[(j + 1) % N][
                                        (i + 1) % N] - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[:h + 1], axis=0)[
                                         (j + 1) % N]) - epsilon)
                            elif k > h and j < i:
                                # 22
                                solver.Add(
                                    t[(i + 1) % N] + d[(i + 1) % N][j] -
                                    t[(q + 1) % N] - d[(q + 1) % N][j] -
                                    T <= infinity *
                                    (2 - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(j + 1) % N]) -
                                    epsilon)
                                # 23
                                solver.Add(
                                    t[(j + 1) % N] + d[(j + 1) % N][i] -
                                    t[(p + 1) % N] - d[(p + 1) % N][i] -
                                    T <= infinity *
                                    (2 - self.Z[h, (i + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(j + 1) % N]) -
                                    epsilon)
                            elif k < h and j > i:
                                # 24
                                solver.Add(t[(i + 1) % N] + d[(i + 1) % N][j] -
                                           t[(q + 1) % N] - d[(q + 1) % N][j] -
                                           T <= infinity *
                                           (2 - self.Z[h, (i + 1) % N] -
                                            np.sum(self.Z[:h + 1], axis=0)[
                                                (j + 1) % N]) - epsilon)
                                # 25
                                solver.Add(t[(j + 1) % N] + d[(j + 1) % N][i] -
                                           t[(p + 1) % N] - d[(p + 1) % N][i] -
                                           T <= infinity *
                                           (2 - self.Z[h, (i + 1) % N] -
                                            np.sum(self.Z[:h + 1], axis=0)[
                                                (j + 1) % N]) - epsilon)
            for h in range(self.mh):
                for k in range(self.mh):
                    if h == k:
                        continue
                    for x1 in range(len(self.hoistTransportMoves[h])):
                        for x2 in range(len(self.hoistTransportMoves[k])):
                            j, _ = self.hoistTransportMoves[h][x1]
                            q, _ = self.hoistTransportMoves[h][x1 - 1]
                            i, _ = self.hoistTransportMoves[k][x2]
                            p, _ = self.hoistTransportMoves[k][x2 - 1]
                            i -= 1
                            p -= 1
                            j -= 1
                            q -= 1
                            if k > h and i == (j + 1) % N:
                                # 26
                                solver.Add(
                                    t[(j + 1) % N] - t[(p + 1) % N] -
                                    d[(p + 1) % N][i] <= infinity *
                                    (3 - y[(j + 1) % N][
                                        (i + 1) % N] - self.Z[h, (j + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(i + 1) % N]) -
                                    epsilon)
                                # 28
                                solver.Add(
                                    t[(j + 1) % N] - t[(p + 1) % N] -
                                    d[(p + 1) % N][i] - T <= infinity *
                                    (2 - self.Z[h, (j + 1) % N] -
                                     np.sum(self.Z[h:], axis=0)[(i + 1) % N]) -
                                    epsilon)
                            elif k < h and (i + 1) % N == j:
                                # 27
                                solver.Add(
                                    t[(i + 1) % N] - t[(q + 1) % N] -
                                    d[(q + 1) % N][j] <= infinity *
                                    (3 - y[(i + 1) % N][
                                        (j + 1) % N] - self.Z[h, (j + 1) % N] -
                                     np.sum(self.Z[:h + 1], axis=0)[
                                         (i + 1) % N]) - epsilon)
                                # 29
                                solver.Add(t[(i + 1) % N] - t[(q + 1) % N] -
                                           d[(q + 1) % N][j] - T <= infinity *
                                           (2 - self.Z[h, (j + 1) % N] -
                                            np.sum(self.Z[:h + 1], axis=0)[
                                                (i + 1) % N]) - epsilon)
        solver.Minimize(T)
        status = solver.Solve()
        if status == pywraplp.Solver.OPTIMAL:
            self.status = "evaluated"
            self.period = round(solver.Objective().Value())
            self.t = [round(var.solution_value()) for var in t]
            self.b = [round(var.solution_value()) for var in b]
            self.a = [[round(var.solution_value()) for var in l] for l in a]
            self.y = [[round(var.solution_value()) for var in l] for l in y]
            Config.evaluated[self.mh] += 1
        else:
            self.status = "not evaluated"
            Config.notEvaluated[self.mh] += 1
            if self.mh == 1 and len(Config.tempList) < 10:
                Config.tempList.append(self.chromosome)
