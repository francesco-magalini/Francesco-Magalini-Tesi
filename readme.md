# Istruzioni

## Installa le librerie richieste

```sh
pip install -r requirements.txt
```

## Esegui il programma principale

```sh
python3 -u main.py
```
