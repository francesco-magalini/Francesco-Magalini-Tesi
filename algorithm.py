# This file contains the implementation of the parts that make up the genetic algorithm:
# Crossover, Mutation, Initial population generation, Repair, etc.
from solution import Solution
from pprint import pprint
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.algorithms.moo.unsga3 import UNSGA3
from pymoo.util.ref_dirs import get_reference_directions
from pymoo.core.problem import ElementwiseProblem
from pymoo.core.sampling import Sampling
from pymoo.core.crossover import Crossover
from pymoo.core.mutation import Mutation
from pymoo.core.repair import Repair
from pymoo.optimize import minimize
from pymoo.termination.default import DefaultMultiObjectiveTermination
from pymoo.core.duplicate import ElementwiseDuplicateElimination
import numpy as np
import json
import time
import multiprocessing
from multiprocessing.pool import ThreadPool
from pymoo.core.problem import StarmapParallelization
from config import Config
import matplotlib.pyplot as plt
from pymoo.util.running_metric import RunningMetricAnimation
from pymoo.indicators.hv import Hypervolume

rg = np.random.default_rng()


# generates a chromosome of length 'size'
# by randomly taking elements without repetition from the 'genList' list
# with some probability 'separator' characters are randomly inserted
def generateChromosome(genesList, size):
    chromosome = rg.choice(genesList, size, replace=False)
    if rg.random() > 0.9 and size > 4:
        d = rg.exponential(scale=4, size=Config.NS)
        d = np.sort(d)[::-1] / d.sum()
        n = rg.choice(range(1, Config.NS + 1), p=d)
        for i in range(n):
            j = rg.integers(1, size - 2)
            k = 0
            while (chromosome[j] == Config.separator
                   or chromosome[j + 1] == Config.separator
                   or chromosome[j - 1] == Config.separator):
                if k > 4:
                    break
                j = rg.integers(1, size - 1)
                k += 1
            chromosome = np.insert(chromosome, j, Config.separator)
    return chromosome


# generates 'size' chromosomes of which
# p1 chromosomes of length N
# p2 chromosomes of length N - 1
# p3 chromosomes of length N - 2 etc.
# with p1 > p2 > p3
def generatePopulation(size):
    if size < 5:
        return np.ndarray(0, dtype=object)
    genes = list(range(1, Config.N + 1))
    d = rg.exponential(scale=4, size=min(Config.N - 2, 1))
    d = np.sort(d)[::-1] / d.sum() * size
    d = d.astype(int)
    d[0] = d[0] + (size - d.sum())
    population = np.full((size, 1), None, dtype=object)
    k = 0
    for i, z in enumerate(d):
        for j in range(z):
            chromosome = generateChromosome(genes, Config.N - i)
            population[k, 0] = Solution(chromosome)
            k += 1
    return population


def repair1(chromosome):
    flag = False
    min_period = Config.inf
    best_solution = None
    n = len(chromosome)
    for i in range(n):
        for j in range(i + 1, n):
            new_chromosome = np.copy(chromosome)
            new_chromosome[i], new_chromosome[j] = new_chromosome[
                j], new_chromosome[i]
            new_solution = Solution(new_chromosome)
            flag2 = checkSolution(new_solution)
            if flag2:
                if new_solution.period < min_period:
                    best_solution = new_solution
                    min_period = new_solution.period
                    flag = True
    return flag, best_solution if flag else None


def repair2(chromosome):
    missing_genes = [x for x in range(1, Config.N + 1) if x not in chromosome]
    flag = False
    if missing_genes:
        substitues = []
        min_value = Config.inf + 1
        for i in range(len(chromosome)):
            if chromosome[i] == Config.separator: continue
            if Config.M[chromosome[i] - 1] < min_value:
                substitues = []
                substitues.append(i)
                min_value = Config.M[chromosome[i] - 1]
            elif Config.M[chromosome[i] - 1] == min_value:
                substitues.append(i)
        missing_genes = [
            x for x in missing_genes if Config.M[x - 1] > min_value
        ]
        if missing_genes:
            gene_position = rg.choice(substitues)
            chromosome[gene_position] = rg.choice(missing_genes)
            flag = True
    return flag, chromosome


def checkSolution(solution):
    flag = False
    if solution.status == "valid":
        solution.evaluate()
        if solution.status == "evaluated":
            flag = True
    elif solution.status == "evaluated":
        flag = True
    return flag


# Definizione del problema
class CustomProblem(ElementwiseProblem):

    def __init__(self, **kwargs):
        super().__init__(n_var=1, n_obj=2, n_ieq_constr=2, **kwargs)

    def _evaluate(self, x, out, *args, **kwargs):
        solution = x[0]
        flag = checkSolution(solution)
        period = solution.period if flag else Config.inf
        mh = solution.mh if flag else Config.inf
        out["F"] = np.array([period, mh], dtype=float)
        out["G"] = np.array([period - (Config.inf - 1), mh - (Config.inf - 1)],
                            dtype=float)


class CustomRepair(Repair):

    def _do(self, problem, X, **kwargs):
        #if Config.repair != 0:
        for k in range(len(X)):
            solution = X[k, 0]
            flag = checkSolution(solution)
            if not flag:
                Config.repairTotal += 1
                if Config.repair == 0: continue
                chromosome = np.copy(solution.chromosome)
                repairFlag = False
                if Config.repair == 2:
                    repairFlag, newSolution = repair1(chromosome)
                    if repairFlag:
                        print("Repair 1 success")
                        Config.repairSuccess += 1
                        X[k, 0] = newSolution
                if not repairFlag:
                    repairFlag, newChromosome = repair2(chromosome)
                    if repairFlag:
                        Config.repairSuccess += 1
                        newSolution = Solution(newChromosome)
                        X[k, 0] = newSolution
        return X


# Definizione della popolazione iniziale
class CustomSampling(Sampling):

    def _do(self, problem, n_samples, **kwargs):
        return generatePopulation(n_samples)


# Definizione della procedura di mutazione (probabilità 0.20)
# Scambio fra geni, aggiunta o eliminazione di un gene
class CustomMutation(Mutation):

    def __init__(self):
        super().__init__()

    def _do(self, problem, X, **kwargs):
        for k in range(len(X)):
            chromosome = X[k, 0].chromosome
            if rg.random() < 0.2:
                p = rg.choice(range(3))
                if p == 1:
                    i, j = rg.choice(range(0, chromosome.size),
                                     size=2,
                                     replace=False)
                    chromosome[i], chromosome[j] = chromosome[j], chromosome[i]
                elif p == 2:
                    i = rg.choice(range(0, chromosome.size))
                    chromosome = np.delete(chromosome, i)
                else:
                    elements = set(range(0, Config.N + 1)) - set(chromosome)
                    if elements:
                        i = rg.choice(range(0, chromosome.size))
                        el = rg.choice(list(elements))
                        chromosome = np.insert(chromosome, i, el)
            X[k, 0] = Solution(chromosome)
        return X


class CustomDuplicateElimination(ElementwiseDuplicateElimination):

    def is_equal(self, a, b):
        return np.array_equal(a.X[0].chromosome, b.X[0].chromosome)


# Definizione della procedura di crossover
class CustomCrossover(Crossover):

    def __init__(self):
        # define the crossover: number of parents and number of offsprings
        super().__init__(2, 2)

    def _do(self, problem, X, **kwargs):
        # The input of has the following shape (n_parents, n_matings, n_var)
        _, n_matings, n_var = X.shape

        # The output owith the shape (n_offsprings, n_matings, n_var)
        # Because there the number of parents and offsprings are equal it keeps the shape of X
        Y = np.full_like(X, None, dtype=object)

        # for each mating provided
        for k in range(n_matings):
            # get the first and the second parent
            P1, P2 = X[0, k, 0].chromosome, X[1, k, 0].chromosome
            O1, O2 = np.copy(P1), np.copy(P2)
            n1, n2 = P1.size, P2.size
            n = min(n1, n2)
            k1 = rg.integers(0, n)
            k2 = rg.integers(0, n)
            i = 0
            while abs(k1 - k2) <= 1:
                k1 = rg.integers(0, n)
                k2 = rg.integers(0, n)
                i += 1
                if (i > 10):
                    raise Exception("Error")
            if k1 > k2:
                k1, k2 = k2, k1
            # generate O1
            indices = np.arange(n1)
            mask = (indices >= k1) & (indices <= k2) & (indices % 2 == 1)
            elements = set(P1[~mask])
            j = 0
            for i in indices:
                if mask[i]:
                    while j < n2:
                        if P2[j] not in elements: break
                        j += 1
                    if j < n2:
                        O1[i] = P2[j]
                        j += 1
                    else:
                        O1[i] = -1
            O1 = O1[O1 != -1]
            # generate O2
            indices = np.arange(n2)
            mask = (indices >= k1) & (indices <= k2) & (indices % 2 == 0)
            elements = set(P2[~mask])
            j = 0
            for i in indices:
                if mask[i]:
                    while j < n1:
                        if P1[j] not in elements: break
                        j += 1
                    if j < n1:
                        O2[i] = P1[j]
                        j += 1
                    else:
                        O2[i] = -1
            O2 = O2[O2 != -1]

            # join the character list and set the output
            Y[0, k, 0], Y[1, k, 0] = Solution(O1), Solution(O2)

        return Y


ref_dirs = get_reference_directions("uniform", 2, n_partitions=6)


def execute():
    start_time = time.time()
    n_threads = 4
    pool = multiprocessing.Pool(Config.n_threads)
    pool = ThreadPool(n_threads)
    runner = StarmapParallelization(pool.starmap)
    problem = CustomProblem(elementwise_runner=runner
                            ) if Config.multithreading else CustomProblem()
    sampling = CustomSampling()
    mutation = CustomMutation()
    eliminate_duplicates = CustomDuplicateElimination()
    crossover = CustomCrossover()
    repair = CustomRepair()
    if Config.algorithm == 'NSGA3':
        algorithm = UNSGA3(ref_dirs=ref_dirs,
                           pop_size=100,
                           sampling=sampling,
                           crossover=crossover,
                           mutation=mutation,
                           eliminate_duplicates=eliminate_duplicates,
                           repair=repair)
    else:
        algorithm = NSGA2(pop_size=100,
                          sampling=sampling,
                          crossover=crossover,
                          mutation=mutation,
                          eliminate_duplicates=eliminate_duplicates,
                          repair=repair)
    termination = DefaultMultiObjectiveTermination(xtol=1e-8,
                                                   cvtol=1e-6,
                                                   ftol=1e-6,
                                                   period=100,
                                                   n_max_gen=1000,
                                                   n_max_evals=100000)
    res = minimize(problem,
                   algorithm, ("n_gen", Config.n_gen),
                   verbose=False,
                   save_history=True)
    solutions = []
    for i, solution in enumerate(res.X):
        sol = solution[0].toDict()
        pprint(sol)
        solutions.append(sol)
    with open(f"output/solution_{Config.name}.json", "w") as f:
        config = {
            "nop": Config.nop,
            "mt": Config.mt,
            "N": Config.N,
            "inf": Config.inf,
            "m": Config.m,
            "M": Config.M,
            "r": Config.r,
            "d": Config.d,
            "name": Config.name,
        }
        json.dump({"configuration": config, "solutions": solutions}, f)
    print(f"\n\nIn {round(time.time() - start_time, 2)} seconds")
    print("Evaluated: ")
    pprint(Config.evaluated)
    print("Not evaluated: ")
    pprint(Config.notEvaluated)
    print(
        f"Total repair procedures: {Config.repairTotal} succesfull: {Config.repairSuccess}"
    )
    #pprint(Config.tempList)
    pool.close()
    """
    F = res.opt.get("F")
    hist = res.history
    n_evals = []  # corresponding number of function evaluations\
    hist_F = []  # the objective space values in each generation
    hist_cv = []  # constraint violation in each generation
    hist_cv_avg = []  # average constraint violation in the whole population

    for algo in hist:

        # store the number of function evaluations
        n_evals.append(algo.evaluator.n_eval)

        # retrieve the optimum from the algorithm
        opt = algo.opt

        # store the least contraint violation and the average in each population
        hist_cv.append(opt.get("CV").min())
        hist_cv_avg.append(algo.pop.get("CV").mean())

        # filter out only the feasible and append and objective space values
        feas = np.where(opt.get("feasible"))[0]
        hist_F.append(opt.get("F")[feas])
    
    # replace this line by `hist_cv` if you like to analyze the least feasible optimal solution and not the population
    vals = hist_cv_avg

    k = np.where(np.array(vals) <= 0.0)[0].min()
    print(
        f"Whole population feasible in Generation {k} after {n_evals[k]} evaluations."
    )

    plt.figure(figsize=(7, 5))
    plt.plot(n_evals, vals, color='black', lw=0.7, label="Avg. CV of Pop")
    plt.scatter(n_evals, vals, facecolor="none", edgecolor='black', marker="p")
    plt.axvline(n_evals[k], color="red", label="All Feasible", linestyle="--")
    plt.title("Convergence")
    plt.xlabel("Function Evaluations")
    plt.legend()
    plt.show()
    
    approx_ideal = F.min(axis=0)
    approx_nadir = F.max(axis=0)
    metric = Hypervolume(ref_point=np.array([1.1, 1.1]),
                         norm_ref_point=False,
                         zero_to_one=True,
                         ideal=approx_ideal,
                         nadir=approx_nadir)

    hv = [metric.do(_F) for _F in hist_F]

    plt.figure(figsize=(7, 5))
    plt.plot(n_evals, hv, color='black', lw=0.7, label="Avg. CV of Pop")
    plt.scatter(n_evals, hv, facecolor="blue", edgecolor='blue', marker="p")
    plt.title("Convergence")
    plt.xlabel("Function Evaluations")
    plt.ylabel("Hypervolume")
    plt.show()
    """
