from collections import defaultdict


class Config:
    name = None
    nop = None
    mt = None
    N = None
    inf = None
    m = None
    M = None
    r = None
    d = None
    separator = None
    NS = None
    repair = 0
    n_gen = 2
    algorithm = 'NSGA2'
    multithreading = False,
    n_threads = 4
    checkCollisions = True
    evaluated = defaultdict(int)
    notEvaluated = defaultdict(int)
    repairTotal = 0
    repairSuccess = 0
    tempList = []